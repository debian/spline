/************************************************************************
 * utils.c -- various utilities						*
 *									*
 * This program is free software; you can redistribute it and/or modify *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2 of the License, or    *
 * (at your option) any later version.                                  *
 *									*
 * This program is distributed in the hope that it will be useful,      *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of       *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the        *
 * GNU General Public License for more details.                         *
 *									*
 * You should have received a copy of the GNU General Public License    *
 * along with this program; if not, write to the Free Software          *
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.            *
 *									*
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "utils.h"

#define LINEFRAGMENT 32

void *xmalloc(size_t size)	/* safe malloc */
{
  void *ptr;

  ptr=malloc(size);
  if (ptr == NULL) {
    perror("xmalloc"); exit(1);
  }

  return ptr;
}

void *xrealloc(void *ptr, size_t size)
{
  ptr=realloc(ptr, size);
  if (ptr == NULL) {
    perror("xrealloc"); exit(1);
  }

  return ptr;
}

char *gettextline(FILE *stream)
/* Read a line of text without imposing an length limit */
/* From: Bison Manual */
{
  char *line;

#ifdef HAVE_GETLINE
  line=NULL;
  size_t len=0;
  ssize_t nread;

  nread=getline(&line, &len, stream);
  if (nread > 0) {
    line[len-1]='\0'; /* remove leading \n */
  } else if (nread == -1) {
    line=NULL;
  }
#else
  int c;
  size_t i;

  line=xmalloc(LINEFRAGMENT+1); i=0;
  while (((c=fgetc(stream)) != EOF) && (c != '\n')) {
    if ((i > 0) && (i % LINEFRAGMENT) == 0) {
      line=xrealloc(line,i+LINEFRAGMENT+1);
    }
    line[i++]=(char)c;
  }
  line[i]='\0';

  if (c == EOF) {
    free(line); line=NULL;
  }
#endif

  return line;
}
