# Makefile for aspline

RM      = rm -f

CC      = gcc
CFLAGS  = -g
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
CFLAGS += -O0
else
CFLAGS += -O2
endif
CFLAGS += -Wall -Wstrict-prototypes -Wmissing-prototypes\
 -Wmissing-declarations -Wbad-function-cast -Wnested-externs\
 -Wcast-qual -Wcast-align -Wshadow -Wwrite-strings -Wpointer-arith\
 -Wformat\
 -Wfloat-equal\
 -mfpmath=sse
# -mfpmath=387
#DEFS	= -D_GNU_SOURCE -DHAVE_GETLINE -DOLD_OUTPUT -DVERSION=\"1.2\"
DEFS	= -D_GNU_SOURCE -DHAVE_GETLINE -DVERSION=\"1.2\"
LIBS    = -lm

INSTOPT=
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
INSTOPT += -s
endif

BIN     = aspline
MAN     = $(BIN).1
SRCS    = utils.c
OBJS	= ${SRCS:.c=.o}

prefix  = /usr
BINDIR  = $(prefix)/bin
MANDIR  = $(prefix)/share/man/man1

all:	$(BIN)

.c.o:	$(SRCS)
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<

$(BIN):	$(SRCS) $(OBJS) $(BIN).c
	$(CC) $(CFLAGS) $(DEFS) -o $(BIN) $(BIN).c $(OBJS) $(LIBS)

clean:
	$(RM) *.o *~ *.bak \#*\# errors core* a.out TAGS *.out

distclean: clean
	$(RM) $(BIN)

realclean:	distclean
	-rcsclean

install:	$(MAN) $(BIN)
	install -d -g root -o root -m 755 $(BINDIR) $(MANDIR)
	install $(INSTOPT) -g root -o root -m 755 $(BIN) $(BINDIR)
	install -g root -o root -m 644 $(MAN) $(MANDIR)
